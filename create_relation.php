<?php
/*
 * create_relation.php
 * Create this file into your root directory of vtiger i.e. vtigercrm/
 * and then run this file directly using your browser
 * for example localhost/vtigercrm/create_relation.php
 * */
include_once('vtlib/Vtiger/Module.php');
$moduleInstance = Vtiger_Module::getInstance('Project');
$accountsModule = Vtiger_Module::getInstance('PurchaseOrder');
$relationLabel  = 'Purchase Order';
$moduleInstance->setRelatedList(
	      $accountsModule, $relationLabel, Array('ADD') //you can do select also Array('ADD','SELECT')
      );

echo "done";
